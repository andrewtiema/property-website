<section class="subscribe">
		<div class="realhome_subscribe">
			<div class="realhome container">
				<h2>Subscribe for Our Newsletter</h2>
				<div class="row align-center">
					<div class="col-lg-6 col-md-6">
						<form class="realhome_form_subscribe mailchimp form-inline" method="post">
							<input type="email" id="subscribeEmail" name="EMAIL" class="form_email" placeholder="Enter Your Email">
							<button type="submit" value="Subscribe">Submit</button>
							<label for="subscribeEmail" class="error"></label>
							<p class="subscription-success"></p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>