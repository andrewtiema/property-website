<?php include 'includes/header.php'; ?>

<!-- START SECTION PROPERTIES LISTING -->
<section class="properties-right list featured portfolio blog">
    <div class="container">
        <div class="row">

            <aside class="col-lg-3 col-md-12 car">
                <div class="widget">
                    <div class="section-heading">
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="media-body">
                                <h5>Search Properties</h5>
                                <div class="border"></div>
                                <p>Search your Properties</p>
                            </div>
                        </div>
                    </div>
                    <!-- Search Fields -->
                    <div class="main-search-field">
                        <h5 class="title">Filter</h5>
                        <form method="GET">
                            <div class="at-col-default-mar">
                                <select>
                                    <option value="0" selected>Location</option>
                                    <option value="1">New York</option>
                                    <option value="2">Los Angeles</option>
                                    <option value="3">Chicago</option>
                                    <option value="4">Philadelphia</option>
                                    <option value="5">San Francisco</option>
                                </select>
                            </div>
                            <div class="at-col-default-mar">
                                <select class="div-toggle" data-target=".my-info-1">
                                    <option value="0" data-show=".acitveon" selected>Property Status</option>
                                    <option value="1" data-show=".sale">For Sale</option>
                                    <option value="2" data-show=".rent">For Rent</option>
                                    <option value="3" data-show=".rent">Sold</option>
                                </select>
                            </div>
                            <div class="at-col-default-mar">
                                <div class="at-col-default-mar">
                                    <select>
                                        <option value="0" selected>Property Type</option>
                                        <option value="1">Family House</option>
                                        <option value="2">Apartment</option>
                                        <option value="3">Condo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="at-col-default-mar">
                                <select>
                                    <option value="0" selected>Beds</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                            <div class="at-col-default-mar">
                                <select>
                                    <option value="0" selected>Baths</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                            <div class="col-lg-12 no-pds">
                                <div class="at-col-default-mar">
                                    <input class="at-input" type="text" name="min-area" placeholder="Squre Fit Min">
                                </div>
                            </div>
                            <div class="col-lg-12 no-pds my-4">
                                <div class="at-col-default-mar">
                                    <input class="at-input" type="text" name="max-area" placeholder="Squre Fit Max">
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Price Fields -->
                    <div class="main-search-field-2">
                        <div class="range-slider">
                            <input type="text" disabled class="slider_amount m-t-lg-30 m-t-xs-0 m-t-sm-10">
                            <div class="slider-range"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 no-pds">
                        <div class="at-col-default-mar">
                            <button class="btn btn-default hvr-bounce-to-right" type="submit">Search</button>
                        </div>
                    </div>


                </div>
            </aside>


            <div class="col-lg-9 col-md-12 blog-pots">
                <!-- Block heading Start-->
                <div class="block-heading">
                    <div class="row">
                        <div class="col-lg-6 col-md-5 col-2">
                            <h4>
                                <span class="heading-icon">
                                <i class="fa fa-th-list"></i>
                                </span>
                                <span class="hidden-sm-down">Properties Listing</span>
                            </h4>
                        </div>
                        <div class="col-lg-6 col-md-7 col-10 cod-pad">
                            <div class="sorting-options">
                                <select class="sorting">
                                    <option>Price: High to low</option>
                                    <option>Price: Low to high</option>
                                    <option>Sells: High to low</option>
                                    <option>Sells: Low to high</option>
                                </select>
                                <a href="properties.php" class="change-view-btn active-view-btn"><i class="fa fa-th-list"></i></a>
                                <a href="properties.php" class="change-view-btn lde"><i
                                            class="fa fa-th-large"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Block heading end -->
                
                <div class="row featured portfolio-items">
                <?php
				$unit = Unit::all();
				// foreach($show as $unit)
				// {
					// $property=Property::find_by_id($unit->property_id);
			        ?>

                    <?php foreach($unit as $entity):?>
                    <div class="item col-lg-5 col-md-12 col-xs-12 landscapes sale pr-0 pb-0 my-44 ft">
                        <div class="project-single mb-0 bb-0">
                            <div class="project-inner project-head">
                                <div class="project-bottom">
                                    <h4><a href="property.php?id=<?=$entity->id;?>">View Property</a><span
                                                class="category">Real Estate</span></h4>
                                </div>
                                <div class="button-effect">
                                    <a href="property.php" class="btn"><i class="fa fa-link"></i></a>
                                    <a href="https://www.youtube.com/watch?v=2xHQqYRcrx4"
                                       class="btn popup-video popup-youtube"><i class="fas fa-video"></i></a>
                                    <a class="img-poppu btn" href="../images/feature-properties/fp-2.jpg"
                                       data-rel="lightcase:myCollection:slideshow"><i class="fa fa-photo"></i></a>
                                </div>
                                <div class="homes">
                                    <!-- homes img -->
                                    <a href="property.php?id=<?=$entity->id;?>" class="homes-img">
                                        <div class="homes-tag button alt featured">Featured</div>
                                        <div class="homes-tag button alt sale">For Sale</div>
                                        <div class="homes-price">Family Home</div>
                                        <img src="<?=$entity->photo_1;?>" alt="home-1"
                                             class="img-responsive">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- homes content -->
                    <div class="col-lg-7 col-md-12 homes-content pb-0 my-44 ft mb-44">
                        <!-- homes address -->
                        <h3><?=$entity->title;?></h3>
                        <p class="homes-address mb-3">
                            <a href="property.php">
                                <i class="fa fa-map-marker"></i><span><?=$entity->title;?></span>
                            </a>
                        </p>
                        <!-- homes List -->
                        <ul class="homes-list clearfix">
                            <li>
                                <i class="fa fa-bed" aria-hidden="true"></i>
                                <span>6 Bedrooms</span>
                            </li>
                            <li>
                                <i class="fa fa-bath" aria-hidden="true"></i>
                                <span>3 Bathrooms</span>
                            </li>
                            <li>
                                <i class="fa fa-object-group" aria-hidden="true"></i>
                                <span>720 sq ft</span>
                            </li>
                            <li>
                                <i class="fas fa-warehouse" aria-hidden="true"></i>
                                <span>2 Garages</span>
                            </li>
                        </ul>
                        <!-- Price -->
                        <div class="price-properties">
                            <h3 class="title mt-3">
                                <a href="property.php"><?=$entity->rent_amount;?></a>
                            </h3>
                            <div class="compare">
                                <a href="#" title="Compare">
                                    <i class="fas fa-exchange-alt"></i>
                                </a>
                                <a href="#" title="Share">
                                    <i class="fas fa-share-alt"></i>
                                </a>
                                <a href="#" title="Favorites">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                            </div>
                        </div>
                        <div class="footer">
                            <a href="../files/agent-details.html">
                                <i class="fa fa-user"></i> Jhon Doe
                            </a>
                            <span>
                            <i class="fa fa-calendar"></i> 2 months ago
                        </span>
                        </div>
                    </div>
                   <?php endforeach;?>
                    <nav aria-label="...">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>

                </div>
            </div>

        </div>

    </div>
</section>


<!-- START FOOTER -->
<?php include 'includes/footer.php'; ?>
<!-- END FOOTER -->
