<?php include 'includes/header.php';?>

	<section class="main-search-field">
		<div class="container">
			<h3>Find Your Dream House</h3>
			<form action="properties.php" method="POST">
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="at-col-default-mar">
							<select>
								<option value="0" selected>Location</option>
								<option value="1">Nairobi</option>
								<option value="2">Kiambu</option>
								<option value="3">Thika</option>
								<option value="4">Mombasa</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="at-col-default-mar">
							<select class="div-toggle" data-target=".my-info-1">
								<option value="0" data-show=".acitveon" selected>Property Status</option>
								<option value="1" data-show=".sale">For Sale</option>
								<option value="2" data-show=".rent">For Rent</option>
								<option value="3" data-show=".rent">Sold</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="at-col-default-mar">
							<div class="at-col-default-mar">
								<select>
									<option value="0" selected>Property Type</option>
									<option value="1">Family House</option>
									<option value="2">Apartment</option>
									<option value="3">Condo</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="at-col-default-mar">
							<select class="div-toggle" data-target=".my-info-1">
								<option value="0" selected>Beds</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-6 b-search__main-form label">
						<input type="text" disabled class="slider_amount m-t-lg-30 m-t-xs-0 m-t-sm-10">
						<div class="slider-range"></div>
					</div>
					
					<div class="col-lg-3 no-pds">
						
					</div>
					<div class="col-lg-3 no-pds">
						
					</div>
					
					<div class="col-lg-3 col-md-6">
						<div class="at-col-default-mar no-mb">
							<button class="btn btn-default hvr-bounce-to-right" type="submit">Search</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
	<!-- END SECTION SEARCH AREA -->

	<!-- START SECTION RECENTLY PROPERTIES -->
	<section class="recently portfolio">
		<div class="container-fluid">
			<div class="section-title">
				<h3>Recently</h3>
				<h2>Properties</h2>
			</div>
			<div class="row portfolio-items">
			<?php
				$show = Unit::all(array('limit'=>4));
				foreach($show as $unit)
				{
					$property=Property::find_by_id($unit->property_id);
			?>
				<div class="item col-lg-3 col-md-6 col-xs-12 landscapes">
					<div class="project-single">
						<div class="project-inner project-head">
							<div class="project-bottom">
							<h4><a href="property.php<?=$unit->photo_1;?>">View Property</a><span class="category">Real Estate</span></h4>
							</div>
							<div class="button-effect">
								<a href="property.php" class="btn"><i class="fa fa-link"></i></a>
								<a href="https://www.youtube.com/watch?v=2xHQqYRcrx4" class="btn popup-video popup-youtube"><i class="fas fa-video"></i></a>
								<a class="img-poppu btn" href="<?=$unit->photo_1?>" data-rel="lightcase:myCollection:slideshow"><i class="fa fa-photo"></i></a>
							</div>
							<div class="homes">
								<!-- homes img -->
								<a href="property.php?id=<?=$unit->id;?>" class="homes-img">
									<div class="homes-tag button alt featured">Featured</div>
									<div class="homes-tag button alt sale">For Sale</div>
									<div class="homes-price">Family Home</div>
									<img style="height:200px;" src="<?=$unit->photo_1?>"  alt="home-1" class="img-responsive">
								</a>
							</div>
						</div>
						<!-- homes content -->
						<div class="homes-content">
							<!-- homes address -->
							<h3><?=$property->title?></h3>
							<p class="homes-address mb-3">
								<a href="property.php">
									<i class="fa fa-map-marker"></i><span><?=$unit->description; ?></span>
								</a>
							</p>
							<!-- homes List -->
							<ul class="homes-list clearfix">
								<li>
									<i class="fa fa-bed" aria-hidden="true"></i>
									<span><?=$unit->no_rooms;?></span>
								</li>
								<li>
									<i class="fa fa-bath" aria-hidden="true"></i>
									<span><?=$unit->has_washroom;?></span>
								</li>
								<li>
									<i class="fa fa-object-group" aria-hidden="true"></i>
									<span>720 sq ft</span>
								</li>
							</ul>
							<!-- Price -->
							<div class="price-properties">
								<h3 class="title mt-3">
                                <a href="property.php"><?=$unit->rent_amount;?></a>
                                </h3>
								<div class="compare">
									<a href="#" title="Compare">
										<i class="fas fa-exchange-alt"></i>
									</a>
									<a href="#" title="Share">
										<i class="fas fa-share-alt"></i>
									</a>
									<a href="#" title="Favorites">
										<i class="fa fa-heart-o"></i>
									</a>
								</div>
							</div>
							<div class="footer">
								<a href="#">
									<i class="fa fa-user"></i> Jhon Doe
								</a>
								<span>
                                <i class="fa fa-calendar"></i> 2 months ago
                            </span>
							</div>
						</div>
					</div>
				</div>


				<?php
	}
?>


			</div>

		</div>
	</section>
	<!-- END SECTION RECENTLY PROPERTIES -->

	<!-- STAR SECTION WELCOME -->
	<section class="welcome">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-12 col-xs-12">
					<div class="welcome-title">
						<h2>WELCOME TO <span>KODI PLUS</span></h2>
						<h4>THE BEST PLACE TO FIND THE HOUSE YOU WANT.</h4>
					</div>
					<div class="welcome-content">
						<p> <span>KODI PLUS</span> is the best place for elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation oris nisi ut aliquip ex ea.</p>
					</div>
					<div class="welcome-services">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-xs-12 ">
								<div class="w-single-services">
									<div class="services-img img-1">
										<img src="images/1.png" width="32" alt="">
									</div>
									<div class="services-desc">
										<h6>Buy Property</h6>
										<p>We have the best properties
											<br> elit, sed do eiusmod tempe</p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12 ">
								<div class="w-single-services">
									<div class="services-img img-2">
										<img src="images/2.png" width="32" alt="">
									</div>
									<div class="services-desc">
										<h6>Rent Property</h6>
										<p>We have the best properties
											<br> elit, sed do eiusmod tempe</p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12 ">
								<div class="w-single-services no-mb mbx">
									<div class="services-img img-3">
										<img src="images/3.png" width="32" alt="">
									</div>
									<div class="services-desc">
										<h6>Real Estate Kit</h6>
										<p>We have the best properties
											<br> elit, sed do eiusmod tempe</p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12 ">
								<div class="w-single-services no-mb">
									<div class="services-img img-4">
										<img src="images/4.png" width="32" alt="">
									</div>
									<div class="services-desc">
										<h6>Sell Property</h6>
										<p>We have the best properties
											<br> elit, sed do eiusmod tempe</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-xs-12">
					<div class="wprt-image-video w50">
						<img alt="image" src="<?=$entities[2]->photo_1;?>">
						<a class="icon-wrap popup-video popup-youtube" href="https://www.youtube.com/watch?v=2xHQqYRcrx4">
							<i class="fa fa-play"></i>
						</a>
						<div class="iq-waves">
							<div class="waves wave-1"></div>
							<div class="waves wave-2"></div>
							<div class="waves wave-3"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- END SECTION WELCOME -->

	<!-- START SECTION SERVICES -->
	<section class="services-home bg-white">
		<div class="container">
			<div class="section-title">
				<h3>Property</h3>
				<h2>Services</h2>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-12 m-top-0 m-bottom-40">
					<div class="service bg-light-2 border-1 border-light box-shadow-1 box-shadow-2-hover">
						<div class="media">
							<i class="fa fa-home bg-base text-white rounded-100 box-shadow-1 p-top-5 p-bottom-5 p-right-5 p-left-5"></i>
						</div>
						<div class="agent-section p-top-35 p-bottom-30 p-right-25 p-left-25">
							<h4 class="m-bottom-15 text-bold-700">Houses</h4>
							<p>Nonec pede justo fringilla vel aliquet nec vulputate eget arcu in enim justo rhoncus ut imperdiet venenatis vitae justo.</p>
							<a class="text-base text-base-dark-hover text-size-13" href="properties.php">Read More <i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 m-top-40 m-bottom-40">
					<div class="service bg-light-2 border-1 border-light box-shadow-1 box-shadow-2-hover">
						<div class="media">
							<i class="fas fa-building bg-base text-white rounded-100 box-shadow-1 p-top-5 p-bottom-5 p-right-5 p-left-5"></i>
						</div>
						<div class="agent-section p-top-35 p-bottom-30 p-right-25 p-left-25">
							<h4 class="m-bottom-15 text-bold-700">Apartments</h4>
							<p>Nonec pede justo fringilla vel aliquet nec vulputate eget arcu in enim justo rhoncus ut imperdiet venenatis vitae justo.</p>
							<a class="text-base text-base-dark-hover text-size-13" href="properties.php">Read More <i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 m-top-40 m-bottom-40 commercial">
					<div class="service bg-light-2 border-1 border-light box-shadow-1 box-shadow-2-hover">
						<div class="media">
							<i class="fas fa-warehouse bg-base text-white rounded-100 box-shadow-1 p-top-5 p-bottom-5 p-right-5 p-left-5"></i>
						</div>
						<div class="agent-section p-top-35 p-bottom-30 p-right-25 p-left-25">
							<h4 class="m-bottom-15 text-bold-700">Commercial</h4>
							<p>Nonec pede justo fringilla vel aliquet nec vulputate eget arcu in enim justo rhoncus ut imperdiet venenatis vitae justo.</p>
							<a class="text-base text-base-dark-hover text-size-13" href="properties.php">Read More <i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- END SECTION SERVICES -->

	<!-- START SECTION FEATURED PROPERTIES -->
	<section class="featured portfolio">
		<div class="container">
			<div class="row">
				<div class="section-title col-md-5">
					<h3>Featured</h3>
					<h2>Properties</h2>
				</div>
			</div>
			<div class="row portfolio-items">
			<?php
				$show = Unit::all(array('limit'=>6));
				foreach($show as $unit)
				{
					$property=Property::find_by_id($unit->property_id);
			?>
				<div class="item col-lg-4 col-md-6 col-xs-12 landscapes sale">
					<div class="project-single">
						<div class="project-inner project-head">
							<div class="project-bottom">
								<h4><a href="property.php?id=<?=$unit->id;?>">View Property</a><span class="category">Real Estate</span></h4>
							</div>
							<div class="button-effect">
								<a href="property.php" class="btn"><i class="fa fa-link"></i></a>
								<a href="https://www.youtube.com/watch?v=2xHQqYRcrx4" class="btn popup-video popup-youtube"><i class="fas fa-video"></i></a>
								<a class="img-poppu btn" href="images/feature-properties/fp-1.jpg" data-rel="lightcase:myCollection:slideshow"><i class="fa fa-photo"></i></a>
							</div>
							<div class="homes">
								<!-- homes img -->
								<a href="property.php?id=<?=$unit->id;?>" class="homes-img">
									<div class="homes-tag button alt featured">Featured</div>
									<div class="homes-tag button alt sale">For Sale</div>
									<div class="homes-price">Family Home</div>
									<img style="height:200px;" src="<?=$unit->photo_2;?>" alt="home-1" class="img-responsive">
								</a>
							</div>
						</div>
						<!-- homes content -->
						<div class="homes-content">
							<!-- homes address -->
							<h3><?=$unit->title;?></h3>
							<p class="homes-address mb-3">
								<a href="property.php">
									<i class="fa fa-map-marker"></i><span><?=$unit->title;?></span>
								</a>
							</p>
							<!-- homes List -->
							<ul class="homes-list clearfix">
								<li>
									<i class="fa fa-bed" aria-hidden="true"></i>
									<span><?=$unit->no_rooms;?></span>
								</li>
								<li>
									<i class="fa fa-bath" aria-hidden="true"></i>
									<span><?=$unit->has_washroom;?></span>
								</li>
								<li>
									<i class="fa fa-object-group" aria-hidden="true"></i>
									<span>720 sq ft</span>
								</li>
							</ul>
							<!-- Price -->
							<div class="price-properties">
								<h3 class="title mt-3">
                                <a href="property.php"><?=$unit->rent_amount;?></a>
                                </h3>
								<div class="compare">
									<a href="#" title="Compare">
										<i class="fas fa-exchange-alt"></i>
									</a>
									<a href="#" title="Share">
										<i class="fas fa-share-alt"></i>
									</a>
									<a href="#" title="Favorites">
										<i class="fa fa-heart-o"></i>
									</a>
								</div>
							</div>
							<div class="footer">
								<a href="#">
									<i class="fa fa-user"></i> Jhon Doe
								</a>
								<span>
                                <i class="fa fa-calendar"></i> 2 months ago
                            </span>
							</div>
						</div>
					</div>
				</div>
				<?php
				}
				?>
			</div>
		</div>
	</section>
	<!-- END SECTION FEATURED PROPERTIES -->




	<!-- START SECTION TOP LOCATION -->
	<section class="top-location">
		<h4>SOLD HOUSES</h4>
		<div class="owl-carousel owl-theme" id="tp-carousel">
			<div class="item">
				<div class="tp-caption">
					<h6>Kileleshwa</h6>
					<strong>Property For Sale</strong>
					<p>Price:&nbsp; Kes.230,000</p>
				</div>
				<img src="images/blog/b-1.jpg" alt="">
			</div>
			<div class="item">
				<div class="tp-caption">
					<h6>Lavington</h6>
					<strong>Property For Rent</strong>
					<p>Price:&nbsp; Kes.230,000</p>
				</div>
				<img src="images/blog/b-2.jpg" alt="">
			</div>
			<div class="item">
				<div class="tp-caption">
					<h6>Westlands</h6>
					<strong>Property For Rent</strong>
					<p>Price:&nbsp; Kes.230,000</p>
				</div>
				<img src="images/blog/b-3.jpg" alt="">
			</div>
			<div class="item">
				<div class="tp-caption">
					<h6>Kiambu</h6>
					<strong>For Rent</strong>
					<p>Price:&nbsp; Kes.230,000</p>
				</div>
				<img src="images/blog/b-4.jpg" alt="">
			</div>
			<div class="item">
				<div class="tp-caption">
					<h6>Highrise</h6>
					<strong>For Rent</strong>
					<p>Price:&nbsp; Kes.230,000</p>
				</div>
				<img src="images/blog/b-5.jpg" alt="">
			</div>
			<div class="item">
				<div class="tp-caption">
					<h6>Lang'ata</h6>
					<strong>For Rent</strong>
					<p>Price:&nbsp; Kes.230,000</p>
				</div>
				<img src="images/blog/b-6.jpg" alt="">
			</div>
			<div class="item">
				<div class="tp-caption">
					<h6>Karen</h6>
					<strong>For Rent</strong>
					<p>Price:&nbsp; Kes.230,000</p>
				</div>
				<img src="images/blog/b-7.jpg" alt="">
			</div>
			<div class="item">
				<div class="tp-caption">
					<h6>Kikuyu</h6>
					<strong>For Rent</strong>
					<p>Price:&nbsp; Kes.230,000</p>
				</div>
				<img src="images/blog/b-8.jpg" alt="">
			</div>
		</div>
	</section>
	<!-- END SECTION TOP LOCATION -->

	<!-- START SECTION BLOG -->
	<section class="blog-section">
		<div class="container">
			<div class="section-title">
				<h3>Latest</h3>
				<h2>News</h2>
			</div>
			<div class="news-wrap">
				<div class="row">
					<div class="col-xl-6 col-md-12 col-xs-12">
						<div class="news-item news-item-sm">
							<a href="#" class="news-img-link">
								<div class="news-item-img">
									<img class="resp-img" src="<?=$entities[0]->photo_2;?>" alt="blog image">
								</div>
							</a>
							<div class="news-item-text">
								<a href="#"><h3>The Real Estate News</h3></a>
								<span class="date">Jun 23, 2018 &nbsp;/&nbsp; By Admin</span>
								<div class="news-item-descr">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
								</div>
								<div class="news-item-bottom">
									<a href="#" class="news-link">Read more...</a>
									<ul class="action-list">
										<li class="action-item"><i class="fa fa-heart"></i> <span>306</span></li>
										<li class="action-item"><i class="fa fa-comment"></i> <span>34</span></li>
										<li class="action-item"><i class="fa fa-share-alt"></i> <span>122</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="news-item news-item-sm mb">
							<a href="#" class="news-img-link">
								<div class="news-item-img">
									<img class="resp-img" src="<?=$entities[1]->photo_2;?>" alt="blog image">
								</div>
							</a>
							<div class="news-item-text">
								<a href="#"><h3>The Real Estate News</h3></a>
								<span class="date">Jun 23, 2018 &nbsp;/&nbsp; By Admin</span>
								<div class="news-item-descr">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
								</div>
								<div class="news-item-bottom">
									<a href="#" class="news-link">Read more...</a>
									<ul class="action-list">
										<li class="action-item"><i class="fa fa-heart"></i> <span>306</span></li>
										<li class="action-item"><i class="fa fa-comment"></i> <span>34</span></li>
										<li class="action-item"><i class="fa fa-share-alt"></i> <span>122</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 col-md-12 col-xs-12">
						<div class="news-item news-item-sm">
							<a href="#" class="news-img-link">
								<div class="news-item-img">
									<img class="resp-img" src="<?=$entities[2]->photo_2;?>" alt="blog image">
								</div>
							</a>
							<div class="news-item-text">
								<a href="#"><h3>The Real Estate News</h3></a>
								<span class="date">Jun 23, 2018 &nbsp;/&nbsp; By Admin</span>
								<div class="news-item-descr">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
								</div>
								<div class="news-item-bottom">
									<a href="#" class="news-link">Read more...</a>
									<ul class="action-list">
										<li class="action-item"><i class="fa fa-heart"></i> <span>306</span></li>
										<li class="action-item"><i class="fa fa-comment"></i> <span>34</span></li>
										<li class="action-item"><i class="fa fa-share-alt"></i> <span>122</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="news-item news-item-sm">
							<a href="#" class="news-img-link">
								<div class="news-item-img">
									<img class="resp-img" src="<?=$entities[3]->photo_2;?>" alt="blog image">
								</div>
							</a>
							<div class="news-item-text">
								<a href="#"><h3>The Real Estate News</h3></a>
								<span class="date">Jun 23, 2018 &nbsp;/&nbsp; By Admin</span>
								<div class="news-item-descr">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
								</div>
								<div class="news-item-bottom">
									<a href="#" class="news-link">Read more...</a>
									<ul class="action-list">
										<li class="action-item"><i class="fa fa-heart"></i> <span>306</span></li>
										<li class="action-item"><i class="fa fa-comment"></i> <span>34</span></li>
										<li class="action-item"><i class="fa fa-share-alt"></i> <span>122</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>







	<?php include 'includes/footer.php';?>
<!-- MAIN JS -->
<script src="js/script.js"></script>
