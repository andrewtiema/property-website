<?php include 'includes/header.php'; ?>

	<section class="headings">
		<div class="text-heading text-center">
			<div class="container">
				<h1>Login</h1>
				<h2><a href="index.html">Home </a> &nbsp;/&nbsp; tenant login</h2>
			</div>
		</div>
	</section>
	<!-- END SECTION HEADINGS -->

	<!-- START SECTION LOGIN -->
	<div id="login">
		<div class="login">
			<form>
				
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" name="email" id="email">
					<i class="icon_mail_alt"></i>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" name="password" id="password" value="">
					<i class="icon_lock_alt"></i>
				</div>
				<div class="clearfix add_bottom_30">
					<div class="checkboxes float-left">
						<label class="container_check">Remember me
							<input type="checkbox">
							<span class="checkmark"></span>
						</label>
					</div>
					<div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Forgot Password?</a></div>
				</div>
				<a href="#0" class="btn_1 rounded full-width">Login</a>
				<div class="text-center add_top_10">New to Kodi Plus? <strong><a href="register_tenant">Register as Tenant</a></strong></div>
			</form>
		</div>
	</div>
	<!-- END SECTION LOGIN -->

	<!-- START SECTION NEWSLETTER -->
	<?php include 'includes/newsletter.php'; ?>
	
	<!-- END SECTION NEWSLETTER -->


<!-- START FOOTER -->
<?php include 'includes/footer.php'; ?>
<!-- END FOOTER -->
