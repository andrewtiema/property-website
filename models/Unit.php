<?php
class Unit extends ActiveRecord\Model
{
    static $table = 'units';
    static $belongs_to=array(
        'Property'=>['Property','class_name'=>'Property','foreign_key'=>'property_id']
    );
}