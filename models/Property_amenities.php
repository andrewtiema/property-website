<?php
class Property_amenities extends ActiveRecord\Model
{
    static $table = "property_amenities";
    static $belongs_to=array(
        'Property'=>['Property','class_name'=>'Property','foreign_key'=>'property_id']
    );
}
