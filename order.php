<?php include 'includes/header.php'; ?>
<?php $id = $_GET['id'];
$unit = Unit::find_by_id($id);
?>

	<!-- START SECTION CONTACT US -->
	<section class="contact-us">
		<div class="container">

			<div class="row">
				<div class="col-lg-8 col-md-12">
					<h3 class="mb-4">Complete Enquiry</h3>
					<form id="contactform" method="POST" action="submit_enquiry.php" class="contact-form" name="contactform" method="post" novalidate>
					        <div id="success" class="successform">
    							<p class="alert alert-success font-weight-bold" role="alert">Your message was sent successfully!</p>
    						</div>
    						<div id="error" class="errorform">
    							<p>Something went wrong, try refreshing and submitting the form again.</p>
    						</div>
					<div class="row">
					    
    					    <div class="col-md-6">
    					        <div class="form-group">
        							<input type="text" required class="form-control input-custom input-full" name="name" placeholder="First Name">
        						</div>
    					    </div>
    					    <div class="col-md-6">
    					       <div class="form-group">
        							<input type="text" required class="form-control input-custom input-full" name="phone" placeholder="Phone">
        						</div>
    					    </div>
    					    
    					    <div class="col-md-6">
    					       <div class="form-group">
        							<input type="text" class="form-control input-custom input-full" name="email" placeholder="Email">
        						</div>
    					    </div>
    					    <div class="col-md-6">
    					        <div class="form-group">
        							<select required="1">
        							    <option value="">-Preferred Payment Mode-</option>
        							    <option value="1">Cash</option>
        							    <option value="2">Mortgage </option>
        							    <option value="3">Payment Plan</option>
        							</select>
        						</div>
    					    </div>
    					    <div class="col-md-6">
    					        <div class="form-group">
        							<select required="1">
        							    <option value="">How did you here about us?</option>
        							    <option value="Twitter">Twitter</option><option value="Facebook">Facebook</option><option value="LinkedIn">LinkedIn</option><option value="Website">Website</option><option value="Referral">Referral</option>
        							</select>
        						</div>
    					    </div>
    					    <div class="col-md-6">
    					        <div class="form-group">
        							<textarea class="form-control textarea-custom input-full" id="ccomment" name="message" required rows="8" placeholder="Message"></textarea>
        						</div>
    					    </div>
    					    <div class="col-md-6">
    					       <button type="submit" id="submit-contact" class="btn btn-primary btn-lg">Submit</button>
    					    </div>
					    
					</div>
					</form>					
				</div>
				<div class="col-lg-4 col-md-12 bgc">
					<div class="call-info">
						<h3>Property Details</h3>
						<p class="mb-5">My Property Details</p>
						<ul>
							<li>
								<div class="info">
									<i class="fa fa-map-marker" aria-hidden="true"></i>
									<p class="in-p"><?=$unit->title;?></p>
								</div>
							</li>
							<li>
								<div class="info">
									<i class="fa fa-money" aria-hidden="true"></i>
									<p class="in-p"><?=$unit->rent_amount;?></p>
								</div>
							</li>
							<li>
								<div class="info">
									<i class="fa fa-home" aria-hidden="true"></i>
									<p class="in-p ti"><?=$unit->no_rooms;?></p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- END SECTION CONTACT US -->



	<?php include 'includes/footer.php';?>



