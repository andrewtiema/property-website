<?php

include_once 'activerecord/ActiveRecord.php';
$cfg = ActiveRecord\Config::instance();
ActiveRecord\Connection::$datetime_format = 'Y-m-d H:i:s';
$cfg->set_model_directory('models');
$cfg->set_connections(
	array(
		'development' => 'mysql://root:@localhost/epesi_rental'
	)
);